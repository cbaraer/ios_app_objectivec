#import <UIKit/UIKit.h>

@interface VideoPlayerViewController : UITableViewController

- (void)loadVideoPlayer:(NSString *)string assetid:(NSString *)assetid assetname:(NSString *)assetname assettype:(NSString *)assettype;

@end
