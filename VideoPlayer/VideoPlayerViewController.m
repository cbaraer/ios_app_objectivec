#import "VideoPlayerViewController.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"
#import "ISCache.h"
@import SenseKit;

///////////////////////////Customized url path////////////////////////////////////

#define URL_DATA @"http://dev.nexeven.se/AgentValidation/iOS/assetList.plist"

//////////////////////////////////////////////////////////////////////////////////

@interface VideoPlayerViewController ()<AVPlayerViewControllerDelegate>

@end
@implementation VideoPlayerViewController {
    AVPlayerViewController      *_playerController;
    AVPlayer                    *_player;
    AVAudioSession              *_session;
    NSDictionary                *_releasePlist;
    NSArray                     *_videoPlist;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    _releasePlist = [NSDictionary dictionaryWithContentsOfURL:[NSURL URLWithString:URL_DATA]];
    _videoPlist = [_releasePlist objectForKey:@"Videos"];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.refreshControl addTarget:self action:@selector(refreshInvoked:forState:) forControlEvents:UIControlEventValueChanged];
   }

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)loadVideoPlayer:(NSString *)string assetid:(NSString *)assetid assetname:(NSString *)assetname assettype:(NSString *)assettype  {
    
    _session = [AVAudioSession sharedInstance];
    [_session setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    AVPlayerItem *item = [AVPlayerItem playerItemWithURL:[NSURL URLWithString: string]];
    
    _player = [AVPlayer playerWithPlayerItem:item];
    _playerController = [AVPlayerViewController new];
    _playerController.player = _player;
    _playerController.delegate = self;
    _playerController.allowsPictureInPicturePlayback = true;
    _playerController.showsPlaybackControls = true;


    
    NECustomMetadata *pair1 = [NECustomMetadata alloc];
    pair1.key = @"AMK1";
    pair1.values = @[@"AMV1", @"AMV11"];
    
    NECustomMetadata *pair2 = [NECustomMetadata alloc];
    pair2.key = @"CMK1";
    pair2.values = @[@"CMV1", @"CMV11"];

    [NESenseKit pluginWithAVPlayer:_playerController.player assetId:assetid serverHost:@"https://sense.nexeven.io" nxeCID:@"BBQCID" assetType:assettype assetName:assetname viewerId:@"jorgenS" sessionAvailableBitrates:@"100000, 200000" assetMetadata:@[pair1] viewerMetadata:@[pair2]];


    [_playerController.player play];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:_player.currentItem];
    
    [self presentViewController:_playerController animated:YES completion:NULL];
}

-(void)itemDidFinishPlaying:(NSNotification *) notification {
   [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(closePlayer) userInfo:nil repeats:NO];
}

- (void)closePlayer {
   [self dismissViewControllerAnimated:NO completion:nil];}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_videoPlist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
    NSDictionary *videoItem = [_videoPlist objectAtIndex:indexPath.row];     
    
    UILabel *mediaTitle = (UILabel *)[cell viewWithTag:1];
    mediaTitle.text = [videoItem valueForKey:@"Title"];
    
    UILabel *mediaDescription = (UILabel *)[cell viewWithTag:2];
    mediaDescription.text = [videoItem valueForKey:@"Description"];
    
    UILabel *mediaDate = (UILabel *)[cell viewWithTag:3];
    mediaDate.text = [videoItem valueForKey:@"Date"];
    
    UILabel *mediaDuration = (UILabel *)[cell viewWithTag:4];
    mediaDuration.text = [videoItem valueForKey:@"Duration"];
    
    NSURL *imageURL = [NSURL URLWithString:[videoItem valueForKey:@"ImageUrl"]];
    NSString *key = [videoItem valueForKey:@"ImageUrl"];
    NSData *data = [ISCache objectForKey:key];
    if (data) {
        UIImage *image = [UIImage imageWithData:data];
        UIImageView *mediaThumb = (UIImageView *)[cell viewWithTag:5];
        [mediaThumb setImage:image];
    }
    else {
        UIImageView *mediaThumb = (UIImageView *)[cell viewWithTag:5];
        [mediaThumb setImage:[UIImage imageNamed:@"image_thumbnail"]];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            [ISCache setObject:data forKey:key];
            UIImage *image = [UIImage imageWithData:data];
            dispatch_sync(dispatch_get_main_queue(), ^{
                UIImageView *mediaThumb = (UIImageView *)[cell viewWithTag:5];
                [mediaThumb setImage:image];
            });
        });
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *videoItem = [_videoPlist objectAtIndex:indexPath.row];      
    [self loadVideoPlayer:[videoItem valueForKey:@"VideoUrl"] assetid:[videoItem valueForKey:@"AssetID"] assetname:[videoItem valueForKey:@"Title"] assettype:[videoItem valueForKey:@"AssetType"]];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) {
        
       UIAlertController *alertctrl =[UIAlertController alertControllerWithTitle:@"No Internet Connection" message:@"You must have an active network connection in order to stream Video" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *connection =[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
             //call Action need to perform
        }];
        
        [alertctrl addAction:connection];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)refreshInvoked:(id)sender forState:(UIControlState)state {
    // Refresh table here...
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self performSelector:@selector(viewDidLoad) withObject:nil];
    [self.tableView reloadData];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(endRefresh) userInfo:nil repeats:NO];
}

- (void)endRefresh {
    [self.refreshControl endRefreshing];
    // show in the status bar that network activity is stoping
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - AVPlayerViewControllerDelegate
- (void)playerViewControllerWillStartPictureInPicture:(AVPlayerViewController *)playerViewController {
    NSLog(@"%s", __FUNCTION__);
}

- (void)playerViewControllerDidStartPictureInPicture:(AVPlayerViewController *)playerViewController {
    NSLog(@"%s", __FUNCTION__);
}

- (void)playerViewController:(AVPlayerViewController *)playerViewController failedToStartPictureInPictureWithError:(NSError *)error {
    NSLog(@"%s", __FUNCTION__);
}

- (void)playerViewControllerWillStopPictureInPicture:(AVPlayerViewController *)playerViewController {
    NSLog(@"%s", __FUNCTION__);
}

- (void)playerViewControllerDidStopPictureInPicture:(AVPlayerViewController *)playerViewController {
    NSLog(@"%s", __FUNCTION__);
}

- (BOOL)playerViewControllerShouldAutomaticallyDismissAtPictureInPictureStart:(AVPlayerViewController *)playerViewController {
    NSLog(@"%s", __FUNCTION__);
    return true;
}

- (void)playerViewController:(AVPlayerViewController *)playerViewController restoreUserInterfaceForPictureInPictureStopWithCompletionHandler:(void (^)(BOOL))completionHandler {
    NSLog(@"%s", __FUNCTION__);
}


@end
